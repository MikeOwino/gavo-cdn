// Get the modal
const modal = document.querySelector(".modal");

// Get the button that opens the modal
const applyBtn = document.querySelector(".apply-btn");

// Get the <span> element that closes the modal
const closeBtn = document.querySelector(".close");

// When the user clicks on the button, open the modal
applyBtn.addEventListener("click", function () {
  modal.style.display = "block";
});

// When the user clicks on <span> (x), close the modal
closeBtn.addEventListener("click", function () {
  modal.style.display = "none";
});

// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
});

// Validate the form before submission
const form = document.querySelector("form");
const nameInput = document.getElementById("name");
const emailInput = document.getElementById("email");
const cvInput = document.getElementById("cv");
const coverLetterInput = document.getElementById("cover-letter");
const submitBtn = document.querySelector("button[type=submit]");

function validateForm() {
  if (
    nameInput.value.trim() === "" ||
    emailInput.value.trim() === "" ||
    cvInput.value.trim() === "" ||
    coverLetterInput.value.trim() === ""
  ) {
    alert("Please fill out all fields before submitting.");
    return false;
  } else {
    return true;
  }
}

submitBtn.addEventListener("click", function (event) {
  if (!validateForm()) {
    event.preventDefault();
  }
});
