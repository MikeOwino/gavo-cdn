(function () {
  var options = {
    whatsapp: "254702948854",
    // facebook: "246152853989129",
    call_to_action: "",
    position: "left",
    button_color: "#54d6eb",
    pre_filled_message: "Hello! GavoFoods Limited.",
    greeting_message:
      "Hello! GavoFoods Limited.",
  };
  var proto = document.location.protocol,
    host = "cdn.lancolatech.co.ke",
    url = proto + "//api." + host;
  var s = document.createElement("script");
  s.type = "text/javascript";
  s.async = true;
  s.src = url + "/socials_plugin.js";
  s.onload = function () {
    WhWidgetSendButton.init(host, proto, options);
  };
  var x = document.getElementsByTagName("script")[0];
  x.parentNode.insertBefore(s, x);
})();
